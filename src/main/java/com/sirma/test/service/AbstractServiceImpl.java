package com.sirma.test.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import com.sirma.test.entity.Loadable;
import com.sirma.test.exception.DuplicationException;
import com.sirma.test.exception.NotFoundException;
import com.sirma.test.model.ModelLoadable;
import com.sirma.test.model.mapper.Mapper;
import com.sirma.test.repository.GenericDAO;

public abstract class AbstractServiceImpl<E extends Loadable<Long>, D extends ModelLoadable<Long>, R extends GenericDAO<E>>
    implements CRUD<E, D> {

  protected R repository;
  protected Mapper<E, D> mapper;
  @Autowired
  protected Validator validator;
  protected AbstractServiceImpl(R repository, Mapper<E, D> mapper) {
    this.repository = repository;
    this.mapper = mapper;
  }

  @Override
  public D create(D d) {
    return mapper.toDto(create(mapper.toEntity(d)));
  }

  @Override
  public D create(D d, Consumer<E> after) {
    E entity = create(mapper.toEntity(d));
    if (after != null) {
      after.accept(entity);
    }
    return mapper.toDto(entity);
  }

  @Override
  public E create(E e) {
    try {
      return repository.save(e);      
    }catch (DataIntegrityViolationException ex) {
      throw new DuplicationException(name());
    }
  }

  @Override
  public void createAll(List<D> d) {
    List<Set<ConstraintViolation<D>>> contrains = new ArrayList<>();
    for (D dto : d) {
      Set<ConstraintViolation<D>> v = validator.validate(dto);
      if (!v.isEmpty()) {
        contrains.add(v);
      }
    }

    if (!contrains.isEmpty()) {
      throw new ConstraintViolationException(
          contrains.stream().flatMap(Set::stream).collect(Collectors.toSet()));
    }
    CRUD.super.createAll(d);
  }

  @Override
  public List<D> retrieves() {
    return mapper.toDto(repository.findAll());
  }

  @Override
  public D retrieve(Loadable<Long> id) {
    return mapper.toDto(read(id));
  }

  @Override
  public void update(D d) {
    E entity = read(d);
    repository.save(mapper.entityUpdate(entity, d));
  }

  @Override
  public E update(Loadable<Long> d, Consumer<E> beforeUpdate) {
    E entity = read(d);
    if (beforeUpdate != null) {
      beforeUpdate.accept(entity);
    }
    return repository.save(entity);
  }

  @Override
  public void delete(Loadable<Long> id) {
    delete(id, null);
  }

  @Override
  public void delete(Loadable<Long> id, Consumer<E> e) {
    E entity = read(id);
    if (e != null) {
      e.accept(entity);
    }
    repository.delete(entity);
  }

  @Override
  public E read(Loadable<Long> id) {
    if (id == null || id.getId() == null) {
      throw new NotFoundException(name());
    }
    return repository.findById(id.getId()).orElseThrow(() -> new NotFoundException(name()));
  }

  @SuppressWarnings("unchecked")
  @Override
  public R getRepository() {
    return repository;
  }

  @Override
  public Mapper<E, D> getMapper() {
    return mapper;
  }

  public abstract String name();
}
