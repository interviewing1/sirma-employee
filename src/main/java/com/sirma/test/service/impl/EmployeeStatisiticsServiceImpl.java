package com.sirma.test.service.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import com.sirma.test.entity.EmployeeStatisiticsEntity;
import com.sirma.test.exception.FileParseException;
import com.sirma.test.model.dto.EmployeeStatisitics;
import com.sirma.test.model.dto.EmployeeStatisitics.EmployeeStatisiticsBuilder;
import com.sirma.test.model.dto.TeamPairStatisitics;
import com.sirma.test.model.mapper.Mapper;
import com.sirma.test.repository.EmployeeStatisiticsRepo;
import com.sirma.test.service.AbstractServiceImpl;
import com.sirma.test.service.EmployeeStatisiticsService;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class EmployeeStatisiticsServiceImpl extends AbstractServiceImpl<EmployeeStatisiticsEntity, EmployeeStatisitics, EmployeeStatisiticsRepo>
		implements EmployeeStatisiticsService {

	public EmployeeStatisiticsServiceImpl(EmployeeStatisiticsRepo repository,
        @Qualifier("employeeStatisitics_mapper") Mapper<EmployeeStatisiticsEntity, EmployeeStatisitics> mapper) {
		super(repository, mapper);
	}

    public void save(MultipartFile file) {
      try {
        List<EmployeeStatisitics> tutorials = csvToEmployeeStatisitcs(file.getInputStream());
        createAll(tutorials);
      } catch (IOException e) {
        throw new FileParseException(e.getMessage());
      }
    }

    private static String[] hEADERS = {"EmpID", "ProjectID", "DateFrom", "DateTo"};

    static BiConsumer<EmployeeStatisiticsBuilder, String> empId =
        (b, v) -> b.empId(Long.parseLong(v));
    static BiConsumer<EmployeeStatisiticsBuilder, String> projectID =
        (b, v) -> b.projectId(Long.parseLong(v));
    static BiConsumer<EmployeeStatisiticsBuilder, String> dateFrom =
        (b, v) -> b.dateFrom(LocalDate.parse(v));
    static BiConsumer<EmployeeStatisiticsBuilder, String> dateTo =
        (b, v) -> b.dateTo("NULL".equals(v) ? null : LocalDate.parse(v));

    private static List<BiConsumer<EmployeeStatisiticsBuilder, String>> builders =
        new ArrayList<BiConsumer<EmployeeStatisiticsBuilder, String>>();

    static {
      builders.add(empId);
      builders.add(projectID);
      builders.add(dateFrom);
      builders.add(dateTo);
    }

    public List<EmployeeStatisitics> csvToEmployeeStatisitcs(InputStream is) {
      try (
          BufferedReader fileReader =
              new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8));
          CSVParser csvParser = new CSVParser(fileReader,
              CSVFormat.Builder.create(CSVFormat.DEFAULT).setHeader(hEADERS).setTrim(true)
                  .setIgnoreEmptyLines(true).build());) {

        List<EmployeeStatisitics> employees = new ArrayList<>();

        Iterable<CSVRecord> csvRecords = csvParser.getRecords();

        for (CSVRecord csvRecord : csvRecords) {
          EmployeeStatisiticsBuilder<?, ?> builder = EmployeeStatisitics.builder();


          for (int i = 0; i < builders.size(); i++) {
            if (csvRecord.isSet(i)) {
              builders.get(i).accept(builder, csvRecord.get(i));
            } else {
              break;
            }
          }
          
          employees.add(builder.build());
        }

        return employees;
      } catch (IOException e) {
        throw new FileParseException(e.getMessage());
      }
    }
	@Override
	public String name() {
      return "employee statisitics";
	}

    @Override
    public Collection<TeamPairStatisitics> readTeamPairStatisitics() {
      List<EmployeeStatisitics> ems = retrieves();
      Map<TeamPairStatisitics, TeamPairStatisitics> pairs = new HashMap<>();
      Map<Long, List<EmployeeStatisitics>> projectTeamMate =
          ems.stream().collect(Collectors.groupingBy(EmployeeStatisitics::getProjectId));

      projectTeamMate.values().forEach(e -> addTeamPairs(pairs, e));

      return pairs.values().stream()
          .sorted(Comparator.comparingLong(TeamPairStatisitics::getTeamDays).reversed())

          .collect(Collectors.toList());
    }

    private void addTeamPairs(Map<TeamPairStatisitics, TeamPairStatisitics> pairs,
        List<EmployeeStatisitics> e) {
      int size = e.size();
      EmployeeStatisitics employee1;
      EmployeeStatisitics employee2;
      TeamPairStatisitics ts;
      TeamPairStatisitics tsTemp;

      for (int i = 0; i < size - 1; i++) {
        employee1 = e.get(i);

        for (int j = i + 1; j < size; j++) {
          employee2 = e.get(j);
          ts = TeamPairStatisitics.builder().employee1(employee1.getEmpId())
              .employee2(employee2.getEmpId()).build();
          tsTemp = pairs.get(ts);
          if (tsTemp == null) {
            pairs.put(ts, ts);
          } else {
            ts = tsTemp;
          }

          ts.addTeamDays(calucalteTeamIntercept(employee1, employee2));
        }

      }
    }

    private long calucalteTeamIntercept(EmployeeStatisitics e1, EmployeeStatisitics e2) {
      LocalDate dt1 = readDateTo(e1);
      LocalDate dt2 = readDateTo(e2);
      LocalDate df1 = e1.getDateFrom();
      LocalDate df2 = e2.getDateFrom();

      if (df1.isAfter(dt2) || df2.isAfter(dt1)) {
        return 0;
      }

      LocalDate start = df1.isAfter(df2) ? df1 : df2;
      LocalDate end = dt1.isAfter(dt2) ? dt2 : dt1;



      return ChronoUnit.DAYS.between(start, end);
    }

    private LocalDate readDateTo(EmployeeStatisitics e) {
      LocalDate dt = e.getDateTo();
      if (dt == null) {
        return LocalDate.now();
      }
      return dt;
    }

}
