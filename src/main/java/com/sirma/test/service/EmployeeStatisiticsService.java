package com.sirma.test.service;

import java.io.InputStream;
import java.util.Collection;
import java.util.List;
import org.springframework.web.multipart.MultipartFile;
import com.sirma.test.entity.EmployeeStatisiticsEntity;
import com.sirma.test.model.dto.EmployeeStatisitics;
import com.sirma.test.model.dto.TeamPairStatisitics;

public interface EmployeeStatisiticsService extends CRUD<EmployeeStatisiticsEntity, EmployeeStatisitics> {

  public void save(MultipartFile file);

  public List<EmployeeStatisitics> csvToEmployeeStatisitcs(InputStream is);

  public Collection<TeamPairStatisitics> readTeamPairStatisitics();
}
