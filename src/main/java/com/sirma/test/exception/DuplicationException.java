package com.sirma.test.exception;

@SuppressWarnings("serial")
public class DuplicationException extends AbstractException {

  public DuplicationException(Object... args) {
    super("ent-dp01", "exeption-duplication-message", args);
  }

  public DuplicationException(String code, Object[] args) {
    super(code, "exeption-duplication-message", args);
  }

  public DuplicationException(String code, String refMessage, Object... args) {
    super(code == null ? "ent-dp01" : code, refMessage, args);
  }
}
