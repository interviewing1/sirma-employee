package com.sirma.test.exception;

@SuppressWarnings("serial")
public class NoMoreCapacityException extends AbstractException {

  public NoMoreCapacityException() {
    super("bus-nmc01", "exeption-nomorecapacity-message", null);
  }

  public NoMoreCapacityException(String code) {
    super(code, "exeption-nomorecapacity-message", null);

  }

  public NoMoreCapacityException(String code, String refMessage) {
    super(code == null ? "bus-nmc01" : code, refMessage, null);
  }

}
