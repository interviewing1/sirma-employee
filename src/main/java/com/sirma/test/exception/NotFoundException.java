package com.sirma.test.exception;

@SuppressWarnings("serial")
public class NotFoundException extends AbstractException {
  private static final String NOTFOUND_CODE = "ent-nf01";
  public NotFoundException(Object... args) {
    super(NOTFOUND_CODE, "exeption-notfound-message", args);
  }

  public NotFoundException() {
    super(NOTFOUND_CODE, "exeption-notfound-message", null);
  }

  public NotFoundException(String code, String refMessage) {
    super(code == null ? NOTFOUND_CODE : code, refMessage, null);
  }

}
