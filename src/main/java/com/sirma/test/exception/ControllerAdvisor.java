package com.sirma.test.exception;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;
import org.springframework.context.MessageSource;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import org.springframework.web.util.WebUtils;
import lombok.extern.log4j.Log4j2;

@Log4j2
@RestControllerAdvice
public class ControllerAdvisor extends ResponseEntityExceptionHandler {
  private final MessageSource messageSource;

  public ControllerAdvisor(MessageSource messageSource) {
    this.messageSource = messageSource;
  }

  @ExceptionHandler(NotAcceptedException.class)
  public ResponseEntity<Object> handleNotAcceptException(NotAcceptedException ex,
      WebRequest request, Locale locale) {

    return new ResponseEntity<>(handlingExeption(ex, locale), HttpStatus.NOT_ACCEPTABLE);
  }

  @ExceptionHandler(NotFoundException.class)
  public ResponseEntity<Object> handleNodataFoundException(NotFoundException ex, WebRequest request,
      Locale locale) {
    return new ResponseEntity<>(handlingExeption(ex, locale), HttpStatus.NOT_FOUND);
  }

  @ExceptionHandler(DuplicationException.class)
  public ResponseEntity<Object> handleDuplicationException(DuplicationException ex,
      WebRequest request, Locale locale) {
    return new ResponseEntity<>(handlingExeption(ex, locale), HttpStatus.NOT_ACCEPTABLE);

  }

  @Override
  protected final ResponseEntity<Object> handleMethodArgumentNotValid(
      MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status,
      WebRequest request) {

    String errorMessages = ex.getBindingResult().getAllErrors().stream()
        .map(DefaultMessageSourceResolvable::getDefaultMessage).collect(Collectors.joining(","));
    return new ResponseEntity<>(errorMap("Validation", errorMessages, true, ex),
        HttpStatus.BAD_REQUEST);
  }

  @Override
  protected ResponseEntity<Object> handleExceptionInternal(Exception ex, @Nullable Object body,
      HttpHeaders headers, HttpStatus status, WebRequest request) {
    boolean catched = true;
    if (HttpStatus.INTERNAL_SERVER_ERROR.equals(status)) {
      request.setAttribute(WebUtils.ERROR_EXCEPTION_ATTRIBUTE, ex, RequestAttributes.SCOPE_REQUEST);
      catched = false;
    }
    String message;
    if (body != null) {
      message = body.toString();
    } else {
      message = ex.getMessage();
    }
    return new ResponseEntity<>(errorMap("SPRING", message, catched, ex), headers, status);
  }

  private Map<String, String> handlingExeption(AbstractException ex, Locale locale) {
    String message;
    if (ex.getArgs().length == 0)
      message = messageSource.getMessage(ex.getRefMessage(), new Object[] {}, locale);
    else
      message = messageSource.getMessage(ex.getRefMessage(), ex.getArgs(), locale);


    return errorMap(ex.getCode(), message, ex.isCatched(), ex);
  }

  private Map<String, String> errorMap(String code, String message, boolean catched, Exception ex) {
    Map<String, String> errorMap = new HashMap<>();

    UUID id = UUID.randomUUID();
    errorMap.put("X-error-id", id.toString());
    errorMap.put("X-error-timestamp", LocalDateTime.now().toString());
    errorMap.put("X-error-code", code);
    errorMap.put("X-error-message", message);

    if (catched) {
      log.debug("Catched Exception with id :{} , code :{} and message :{}", id, code, message);

    } else {
      log.error("Uncatched Exception with id :{} , code :{} , message :{} ,and exeption: {}", id,
          code, message, ex);
    }
    return errorMap;
  }
}
