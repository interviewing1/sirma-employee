package com.sirma.test.exception;

@SuppressWarnings("serial")
public class NotAcceptedException extends AbstractException {

  public NotAcceptedException() {
    super("ent-na01", "exeption-notaccepted-field-message", null);
  }

  public NotAcceptedException(String code) {
    super(code, "exeption-notaccepted-field-message", null);

  }

  public NotAcceptedException(String code, String refMessage) {
    super(code == null ? "ent-na01" : code, refMessage, null);
  }
}
