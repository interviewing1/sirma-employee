package com.sirma.test.exception;

import lombok.Getter;

@SuppressWarnings("serial")
@Getter
public abstract class AbstractException extends RuntimeException {
  private final transient Object[] args;
  private final String refMessage;
  private final String code;

  private final boolean catched;

  protected AbstractException(String code, String messageCode, Object[] args) {
    this.code = code;
    this.refMessage = messageCode;
    catched = true;
    this.args = args;
  }

  protected AbstractException(String code, String messageCode, boolean catched, Object[] args) {
    this.code = code;
    this.refMessage = messageCode;
    this.catched = catched;
    this.args = args;
  }

  protected AbstractException(String code, String messageCode, String message, Object[] args) {
    super(message);
    this.code = code;
    this.refMessage = messageCode;
    catched = true;
    this.args = args;
  }

  protected AbstractException(String code, String messageCode, String message, boolean catched,
      Object[] args) {
    super(message);
    this.code = code;
    this.refMessage = messageCode;
    this.catched = catched;
    this.args = args;
  }

}
