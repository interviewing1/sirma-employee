package com.sirma.test.exception;

@SuppressWarnings("serial")
public class FileParseException extends AbstractException {
  private static final String FILE_PARSE_CODE = "ent-nf05";
  public FileParseException(Object... args) {
    super(FILE_PARSE_CODE, "exeption-file-parse-message", args);
  }

  public FileParseException() {
    super(FILE_PARSE_CODE, "exeption-file-parse-message", null);
  }

  public FileParseException(String code, String refMessage) {
    super(code == null ? FILE_PARSE_CODE : code, refMessage, null);
  }

}
