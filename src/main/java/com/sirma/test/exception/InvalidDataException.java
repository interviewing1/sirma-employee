package com.sirma.test.exception;

public class InvalidDataException extends AbstractException {

  private static final long serialVersionUID = -861424844875925976L;

  public InvalidDataException(String code, String messageCode) {
    super(code, messageCode, null);
	}
}