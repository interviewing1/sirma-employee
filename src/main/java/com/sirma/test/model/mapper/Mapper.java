package com.sirma.test.model.mapper;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import com.sirma.test.entity.Loadable;
import com.sirma.test.model.ModelLoadable;

public interface Mapper<E extends Loadable<?>, D extends ModelLoadable<?>> {

	E entityUpdate(E entity, D dto);

	D toDto(E entity);

	E toEntity(D dto);

	default List<D> toDto(Collection<E> entities) {
		if (entities == null || entities.isEmpty()) {
			return Collections.emptyList();
		}
		return entities.stream().map(this::toDto).collect(Collectors.toList());
	}

	default Set<D> toDto(Set<E> entities) {
		if (entities == null || entities.isEmpty()) {
			return Collections.emptySet();
		}
		return entities.stream().map(this::toDto).collect(Collectors.toSet());
	}

	default Set<E> toEntity(Set<D> dto) {
		if (dto == null || dto.isEmpty()) {
			return Collections.emptySet();
		}
		return dto.stream().map(this::toEntity).collect(Collectors.toSet());
	}

	default List<E> toEntity(Collection<D> dto) {
		if (dto == null || dto.isEmpty()) {
			return Collections.emptyList();
		}
		return dto.stream().map(this::toEntity).collect(Collectors.toList());
	}
}
