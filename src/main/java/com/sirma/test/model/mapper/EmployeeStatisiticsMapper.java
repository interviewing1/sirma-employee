package com.sirma.test.model.mapper;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import com.sirma.test.entity.EmployeeStatisiticsEntity;
import com.sirma.test.model.dto.EmployeeStatisitics;

@Component
@Qualifier("employeeStatisitics_mapper")
public class EmployeeStatisiticsMapper implements Mapper<EmployeeStatisiticsEntity, EmployeeStatisitics> {

	@Override
	public EmployeeStatisiticsEntity entityUpdate(EmployeeStatisiticsEntity entity, EmployeeStatisitics dto) {
      entity.setProjectId(dto.getProjectId());
      entity.setEmpId(dto.getEmpId());
      entity.setDateFrom(dto.getDateFrom());
      entity.setDateTo(dto.getDateTo());
      return entity;
	}

	@Override
	public EmployeeStatisitics toDto(EmployeeStatisiticsEntity entity) {
      return EmployeeStatisitics.builder().id(entity.getId()).empId(entity.getEmpId())
          .projectId(entity.getProjectId()).dateFrom(entity.getDateFrom())
          .dateTo(entity.getDateTo()).createdDate(entity.getCreatedDate())
          .lastModifiedDate(entity.getLastModifiedDate()).build();
	}

	@Override
	public EmployeeStatisiticsEntity toEntity(EmployeeStatisitics dto) {
      return EmployeeStatisiticsEntity.builder().empId(dto.getEmpId()).projectId(dto.getProjectId())
          .dateFrom(dto.getDateFrom()).dateTo(dto.getDateTo()).build();
	}

}
