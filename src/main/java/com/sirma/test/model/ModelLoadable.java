package com.sirma.test.model;

import java.io.Serializable;
import com.sirma.test.entity.Loadable;

public interface ModelLoadable<T extends Serializable> extends Loadable<T> {
	void setId(T id);
}
