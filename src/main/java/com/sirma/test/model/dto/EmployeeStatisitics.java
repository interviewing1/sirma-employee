package com.sirma.test.model.dto;

import java.time.LocalDate;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.PastOrPresent;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.sirma.test.model.AbstractAuditableModel;
import com.sirma.test.validator.EmployeeDataValidator;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModelProperty.AccessMode;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@EmployeeDataValidator
public class EmployeeStatisitics extends AbstractAuditableModel<Long> {

  @ApiModelProperty(hidden = true, accessMode = AccessMode.READ_ONLY)
  @JsonProperty(index = 0, access = JsonProperty.Access.READ_ONLY)
  private Long id;

  @JsonProperty(index = 1)
  @Min(value = 0)
  private long empId;

  @JsonProperty(index = 2)
  @Min(value = 0)
  private long projectId;
  @NotNull
  @Past
  private LocalDate dateFrom;

  @PastOrPresent
  private LocalDate dateTo;

}
