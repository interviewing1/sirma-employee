package com.sirma.test.model.dto;

import java.util.Objects;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class TeamPairStatisitics {

  private long employee1;
  private long employee2;

  private long teamDays;



  @Override
  public int hashCode() {
    return Objects.hash(employee1, employee2);
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null)
      return false;

    if (this.getClass() != obj.getClass())
      return false;

    TeamPairStatisitics o = (TeamPairStatisitics) obj;
    return (this == obj) || (o.employee1 == employee1 && o.employee2 == employee2)
        || (o.employee1 == employee2 && o.employee2 == employee1);

  }


  public void addTeamDays(long days) {
    teamDays += days;
  }


}
