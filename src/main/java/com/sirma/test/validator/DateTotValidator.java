package com.sirma.test.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import com.sirma.test.model.dto.EmployeeStatisitics;

public class DateTotValidator
    implements ConstraintValidator<EmployeeDataValidator, EmployeeStatisitics> {

  @Override
  public boolean isValid(EmployeeStatisitics value, ConstraintValidatorContext context) {

    if (value.getDateFrom() == null) {
      return false;
    }
    if (value.getDateTo() == null) {
      return true;
    }

    return value.getDateFrom().isBefore(value.getDateTo());

  }

}
