package com.sirma.test.repository;

import org.springframework.stereotype.Repository;
import com.sirma.test.entity.EmployeeStatisiticsEntity;

@Repository
public interface EmployeeStatisiticsRepo extends GenericDAO<EmployeeStatisiticsEntity> {

}
