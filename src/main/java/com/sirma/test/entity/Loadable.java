package com.sirma.test.entity;

public interface Loadable<T> {
	T getId();
}
