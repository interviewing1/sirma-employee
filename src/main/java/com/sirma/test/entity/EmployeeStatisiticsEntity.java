package com.sirma.test.entity;

import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.EqualsAndHashCode.Include;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity(name = "employee_statistics")
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = {"empId", "projectId"})})
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
public class EmployeeStatisiticsEntity extends AbstractAuditableEntity<Long> {

	@Id
	@Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "statistics_seq")
    @SequenceGenerator(name = "statistics_seq", sequenceName = "statistics_seq", allocationSize = 1)
	private Long id;

    @Column(unique = false, updatable = false)
    @Include
    private long empId;

    @Column(unique = false, updatable = false)
    @Include
    private long projectId;


    private LocalDate dateFrom;

    private LocalDate dateTo;



}
