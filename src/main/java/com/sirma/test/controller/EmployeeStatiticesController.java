package com.sirma.test.controller;

import java.util.Collection;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import com.sirma.test.model.dto.EmployeeStatisitics;
import com.sirma.test.model.dto.TeamPairStatisitics;
import com.sirma.test.service.EmployeeStatisiticsService;
import com.sirma.test.utils.Utils;

@RestController
@RequestMapping(EmployeeStatiticesController.PATH)
public class EmployeeStatiticesController extends AbstractCRUDController<EmployeeStatisitics> {
  public static final String PATH = "v1/employee/statisicis";

	public EmployeeStatiticesController(EmployeeStatisiticsService service) {
      super(service);
	}

    @Override
    public String controllerPath() {
      return PATH;
    }

    @GetMapping(path = "/pairs")
    public Collection<TeamPairStatisitics> readPairs() {
      return ((EmployeeStatisiticsService) service).readTeamPairStatisitics();
    }
    @PostMapping(path = "/csv/upload", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity uploadFile(
        @RequestParam("file") @RequestPart(required = true) MultipartFile file) {

      if (Utils.hasCSVFormat(file)) {
        try {
          ((EmployeeStatisiticsService) service).save(file);

          return ResponseEntity.ok().build();
        } catch (Exception e) {
          return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).build();
        }
      }

      return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }

}
