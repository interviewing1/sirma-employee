package com.sirma.test.controller;

import static org.springframework.http.ResponseEntity.created;
import static org.springframework.http.ResponseEntity.noContent;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import com.sirma.test.model.ModelLoadable;
import com.sirma.test.service.CRUD;

public abstract class AbstractCRUDController<T extends ModelLoadable<Long>> {

	protected final CRUD<?, T> service;

	@Autowired
    protected AbstractCRUDController(CRUD<?, T> service) {
		this.service = service;
	}

    @GetMapping()
	public List<T> all() {
		return service.retrieves();
	}

	@GetMapping("/{id}")
	public T get(@PathVariable("id") Long queriesId) {
		return this.service.retrieve(() -> queriesId);
	}

	@PostMapping
    public ResponseEntity<Void> save(@RequestBody @Valid T form, HttpServletRequest request) {
		T dto = service.create(form);
		return created(ServletUriComponentsBuilder.fromContextPath(request).path(controllerPath() + "/{id}")
				.buildAndExpand(dto.getId()).toUri()).build();
	}

	@PutMapping("/{id}")
    public ResponseEntity<Void> update(@PathVariable("id") Long id, @RequestBody @Valid T form) {
		form.setId(id);
		service.update(form);
		return noContent().build();
	}

	@DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable("id") Long id) {
		service.delete(() -> id);
		return noContent().build();
	}

	public abstract String controllerPath();
}
