package com.sirma.test.config;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.introspect.JacksonAnnotationIntrospector;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;

@Configuration
@EnableJpaAuditing
public class JpaConfig {

  @Bean
  public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
    return new PersistenceExceptionTranslationPostProcessor();
  }

  @Bean
  public ObjectMapper objectMapper() {
    JavaTimeModule module = new JavaTimeModule();
    LocalDateDeserializer localDateTimeDeserializer =
        new LocalDateDeserializer(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
    module.addDeserializer(LocalDate.class, localDateTimeDeserializer);

    return new ObjectMapper()

        .setAnnotationIntrospector(new JacksonAnnotationIntrospector()).registerModule(module)
        // .setDateFormat(new StdDateFormat())
        .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
  }
}
