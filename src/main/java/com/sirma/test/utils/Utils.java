package com.sirma.test.utils;

import org.springframework.web.multipart.MultipartFile;
import lombok.experimental.UtilityClass;

@UtilityClass
public class Utils {

  private static String TYPE = "text/csv";

  public static boolean hasCSVFormat(MultipartFile file) {
    return TYPE.equals(file.getContentType());
  }
}
