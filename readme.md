# Employee Pairs .

## About

### **Pair of employees that have worked as a team for the longest time in days**

Spring boot application take information from api then analize it .

## technologies

- Spring boot 2.4.1
- Spring jpa with h2 DB
- spring-boot-starter-validation
- org.apache.commons
- io.springfox
## Features

- Crud operation add employee with project and date .

- Import from CSV file list of employees data . (data should be valid)

- load statisitics about pairs employee sorted by Longest time .

## Validations 

- Employee id never be negative .

- Project id never be negative . 

- Employee and Project can't accept dublications .

- DateFrom can't be Null .

- DateFrom can't be future .

- DateFrom can't be newest that DateTo .

- DateTo accpet past or present not future .

## Data Example 

| EmpID | ProjectID | DateFrom | DateTo | comment |
| ------ | ------ | ------ | ------ | ------ |
| 115 | 2 |2009-01-01 |2019-01-01 |  employee 115 spend 10 years in this project 5 years teammate with 116 |
| 116 | 2 |2014-01-01 |2019-01-01 |  employee 116 spend 5 years in this project 1 years teammate with 119 |
| 117 | 2 |2019-01-01 |NULL | employee 117 still working until now  in this project 1 years teammate with 118 and team mate with 119 until now  |
| 118 | 2 |2019-01-01 |2020-01-01 | |
| 119 | 2 |2018-01-01 |NULL | |

## get stating 

**this is spring boot application run the jar and it will work on port 8080**

### the API has swagger UI .
`http://localhost:8080/swagger-ui/index.html#/`


### to upload file use v1/employee/statisicis/csv/upload
`curl -X POST "http://localhost:8080/v1/employee/statisicis/csv/upload" -H "accept: */*" -H "Content-Type: multipart/form-data" -F "file=@data.csv;type=text/csv"`

### to see the statisicis pair use v1/employee/statisicis/pairs
`curl -X GET "http://localhost:8080/v1/employee/statisicis/pairs" -H "accept: */*"`

## Notes

**we added csv file on the code for testing**

**please don't added header in csv file**


## Example

### input file 

```
115, 2, 2009-01-01, 2019-01-01
116, 2, 2014-01-01, 2019-01-01
117, 2, 2019-01-01, NULL
118, 2, 2019-01-01, 2020-01-01
119, 2, 2018-01-01, NULL
143, 12, 2013-11-01, 2014-01-05
218, 10, 2012-05-16, NULL
143, 10, 2009-01-01, 2011-04-27
12,	1, 2019-01-01, 2020-01-01
13,	1, 2019-01-01, 2021-12-01
14,	1, 2019-01-01, 2020-12-01
```


### out put pairs 

```
[
  {
    "employee1": 115,
    "employee2": 116,
    "teamDays": 1826
  },
  {
    "employee1": 117,
    "employee2": 119,
    "teamDays": 1071
  },
  {
    "employee1": 13,
    "employee2": 14,
    "teamDays": 700
  },
  {
    "employee1": 12,
    "employee2": 13,
    "teamDays": 365
  },
  {
    "employee1": 117,
    "employee2": 118,
    "teamDays": 365
  },
  {
    "employee1": 118,
    "employee2": 119,
    "teamDays": 365
  },
  {
    "employee1": 12,
    "employee2": 14,
    "teamDays": 365
  },
  {
    "employee1": 116,
    "employee2": 119,
    "teamDays": 365
  },
  {
    "employee1": 115,
    "employee2": 119,
    "teamDays": 365
  },
  {
    "employee1": 116,
    "employee2": 117,
    "teamDays": 0
  },
  {
    "employee1": 115,
    "employee2": 117,
    "teamDays": 0
  },
  {
    "employee1": 116,
    "employee2": 118,
    "teamDays": 0
  },
  {
    "employee1": 115,
    "employee2": 118,
    "teamDays": 0
  },
  {
    "employee1": 218,
    "employee2": 143,
    "teamDays": 0
  }

]
```


